using NUnit.Framework;
using Prime.Services;

namespace Prime.UnitTests.Services
{
    [TestFixture]
    public class PrimeService_IsPrimeShould
    {
        private readonly PrimeService _primeService = new PrimeService();

        [Test]
        public void ValueIsUpper()
        {
            var result = _primeService.PrimeName("fuzz");

            Assert.AreEqual("FUZZ", result);
        }

        [Test]
//case1
//case2
//case3
//case4
        public void ValueIsUpperIncludingSpaces()
        {
            var result = _primeService.PrimeName("fuzz pavlov");

            Assert.AreEqual("FUZZ PAVLOV", result);
        }
    }
}